﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Asphalt;
using BetterLink.injection;
using Eco.Core.Plugins.Interfaces;
using Eco.Gameplay.Components;
using Eco.Gameplay.Objects;
using Harmony;

namespace BetterLink
{
    [AsphaltPlugin]
    public class BetterLink : IModKitPlugin
    {
        public static string prefix = "BetterLink: ";

        public void OnPostEnable()
        {
            printMessage("Plugin started.");

            Asphalt.Api.Asphalt.Harmony.Patch(LinkComponentInjection.TargetMethod(), new HarmonyMethod(typeof(LinkComponentInjection), "Prefix"));
            printMessage("Plugin loaded.");

            relinkLinkedComponents();
        }

        public void relinkLinkedComponents()
        {
            LinkComponent.MaxConnectRadius = 15;


            printMessage("Patching all linked objects");
            LinkComponent.MaxConnectRadius = 15f;
            var allLinkable = WorldObjectManager.All.Where(o => o.HasComponent(typeof(LinkComponent))).ToList();
            foreach (var o in allLinkable)
            {
                try
                {
                    var comp = o.GetComponent<LinkComponent>();
                    var field = typeof(LinkComponent)
                        .GetRuntimeFields()
                        .FirstOrDefault(f => f.Name.Contains("ConnectRadius"));


                    if (field == null)
                    {
                        BetterLink.printMessage("Unable to find ConnectRadius field available:");
                        comp.GetType().GetRuntimeFields().ToList().ForEach(f => Console.WriteLine(f.Name));
                        continue;
                    }

                    field.SetValue(comp, 15);

                    var method = comp.GetType().GetMethods(BindingFlags.Instance | BindingFlags.NonPublic).FirstOrDefault(m => m.Name.Contains("Relink"));

                    if (method == null)
                    {
                        BetterLink.printMessage("Unable to find Relink method");
                        continue;
                    }

                    method.Invoke(comp, new object[] { });
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error while etablish link from betterlink !");
                    Console.WriteLine(e);
                    Console.WriteLine(e.StackTrace);

                }

            }
            printMessage("Patch finished " + allLinkable.Count);
        }

        public string GetStatus()
        {
            return "BetterLink started";
        }

        public static void printMessage(string message)
        {
            Console.WriteLine($"{prefix} {message}");
        }
    }
}

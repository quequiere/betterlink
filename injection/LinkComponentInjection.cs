﻿using Eco.Gameplay.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Harmony;

namespace BetterLink.injection
{
    public class LinkComponentInjection
    {
        public static MethodBase TargetMethod()
        {
            var method = typeof(LinkComponent)
                .GetMethods(BindingFlags.Instance | BindingFlags.NonPublic).FirstOrDefault(m => m.Name.Contains("Relink"));
            return method;
        }

        public static void Prefix(LinkComponent __instance)
        {
            try
            {
                var field = typeof(LinkComponent)
                    .GetRuntimeFields()
                    .FirstOrDefault(f => f.Name.Contains("ConnectRadius"));

                field.SetValue(__instance, 15);
            }
            catch (Exception e)
            {
               BetterLink.printMessage("Error while increase link");
               Console.WriteLine(e.StackTrace);
            }

        }

 

    }
}
